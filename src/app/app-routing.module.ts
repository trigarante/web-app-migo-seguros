import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { DashboardPageComponent } from './modulos/dashboard/containers/dashboard-page/dashboard-page.component';
import { AuthGuard } from './modulos/auth/guards';
import { NotFoundComponent } from './modulos/not-found/not-found.component';
import { DetalleRegistroComponent } from './modulos/polizas/components/detalle-registro/detalle-registro.component';
import { ViewerPdfComponent } from './modulos/polizas/components/viewer-pdf/viewer-pdf.component';
import { AvisoDePrivacidadComponent } from './modulos/aviso-de-privacidad/aviso-de-privacidad.component';

const routes: Routes = [
  // { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'dashboard',
    canActivate: [AuthGuard],
    component: DashboardPageComponent
  },
  {
    path: 'aviso-de-privacidad',
    component: AvisoDePrivacidadComponent
  },
  {
    path: 'polizas',
    // pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modulos/polizas/polizas.module').then(m => m.PolizasModule)
  },
  {
    path: 'solicitudes',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modulos/solicitudes/solicitudes.module').then(m => m.SolicitudesModule)
  },
  {
    path: 'solicitar-modificaciones',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modulos/solicitar-modificaciones/solicitar-modificaciones.module').then(m => m.SolicitarModificacionesModule)
  },
  {
    path: 'datos-personales',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modulos/datos-personales/datos-personales.module').then(m => m.DatosPersonalesModule)
  },
  {
    path: 'aclaracion-y-sugerencias',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modulos/aclaracion-y-sugerencias/aclaracion-y-sugerencias.module').then(m => m.AclaracionYSugerenciasModule)
  },
  // ---------------------
  {
    path: 'ver-poliza-pdf/:idArchivo/:idCliente/:idRegistro',
    canActivate: [AuthGuard],
    component: ViewerPdfComponent
  },
  {
    path: 'detalle-registro',
    canActivate: [AuthGuard],
    component: DetalleRegistroComponent
  },
  // -------------------------
  {
    path: 'instrucciones-de-pago',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modulos/instrucciones-de-pago/instrucciones-de-pago.module').then(m => m.InstruccionesDePagoModule)
  },
  {
    path: 'catalogo-de-facturacion',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modulos/catalogo-de-facturacion/catalogo-de-facturacion.module').then(m => m.CatalogoDeFacturacionModule)
  },
  {
    path: 'identificacion-oficial',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modulos/identificacion-oficial/identificacion-oficial.module').then(m => m.IdentificacionOficialModule)
  },
  {
    path: 'inspeccion-vehicular',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modulos/inspeccion-vehicular/inspeccion-vehicular.module').then(m => m.InspeccionVehicularModule)
  },
  {
    path: 'cancelar-poliza',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modulos/cancelar-una-poliza/cancelar-una-poliza.module').then(m => m.CancelarUnaPolizaModule)
  },
  {
    path: 'caso-de-siniestro',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modulos/siniestros/siniestros.module').then(m => m.SiniestrosModule)
  },
  {
    path: 'condiciones-generales',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modulos/condiciones-generales/condiciones-generales.module').then(m => m.CondicionesGeneralesModule)
  },
  {
    path: 'ajustes',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modulos/ajustes/ajustes.module').then(m => m.AjustesModule)
  },
  {
    path: '',
    loadChildren: () => import('./modulos/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'forgot-password/:data',
    loadChildren: () => import('./modulos/forgot-password/forgot-password.module').then(m => m.ForgotPasswordModule)
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: '404'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
