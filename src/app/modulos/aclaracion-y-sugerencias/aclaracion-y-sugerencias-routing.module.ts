import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AclaracionYSugerenciasPageComponent} from "./containers";

const routes: Routes = [
  {path: '',
    component: AclaracionYSugerenciasPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AclaracionYSugerenciasRoutingModule { }
