import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AclaracionYSugerenciasRoutingModule } from './aclaracion-y-sugerencias-routing.module';
import { AclaracionYSugerenciasComponent } from './components/aclaracion-y-sugerencias/aclaracion-y-sugerencias.component';
import { AclaracionYSugerenciasPageComponent } from './containers/aclaracion-y-sugerencias-page/aclaracion-y-sugerencias-page.component';
import {SharedModule} from "../../shared/shared.module";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatCardModule} from "@angular/material/card";


@NgModule({
  declarations: [AclaracionYSugerenciasComponent, AclaracionYSugerenciasPageComponent],
    imports: [
        CommonModule,
        AclaracionYSugerenciasRoutingModule,
        SharedModule,
        MatToolbarModule,
        MatCardModule
    ]
})
export class AclaracionYSugerenciasModule { }
