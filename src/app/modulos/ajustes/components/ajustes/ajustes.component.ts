import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ClienteService } from 'src/app/@core/services/cliente.service';
import { Usuario } from 'src/app/modulos/auth/models/usuario';
import { AuthService } from "../../../auth/services";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ajustes',
  templateUrl: './ajustes.component.html',
  styleUrls: ['./ajustes.component.scss']
})
export class AjustesComponent implements OnInit {

  hide = true;
  hide2 = true;

  formGroup: FormGroup;
  confirmChangePass: boolean = false;
  static clic: number = 0;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private clienteService: ClienteService) { }

  ngOnInit() {
    this.startBuildForm();
  }

  passwordsIguales(pass1Name: string, pass2Name: string) {

    return (formGroup: FormGroup) => {

      const pass1Control = formGroup.get(pass1Name);
      const pass2Control = formGroup.get(pass2Name);

      if (pass1Control.value === pass2Control.value) {
        pass2Control.setErrors(null);
      } else {
        pass2Control.setErrors({ noEsIgual: true });
      }
    };
  }

  confirmPassword() {
    if (this.formGroup.valid) {
      if (this.passwordField.value === this.confirmPasswordField.value) {
        this.confirmChangePass = true;
        AjustesComponent.clic++;
        if (AjustesComponent.clic > 1) {
          if (this.currentPasswordField.value === '' || this.currentPasswordField.value.trim() === '') {
            alert('El campo de contraseña actual es obligatorio');
          } else {
            const userID = parseInt(JSON.parse(localStorage.getItem('token')).id);
            this.authService.loginUser(userID, this.currentPasswordField.value, this.confirmPasswordField.value).subscribe({
              next: (response) => { },
              error: (error) => {
                console.error('Error al actualizar la contraseña => ', error);
                Swal.fire({
                  title: 'Error al Actualizar',
                  icon: 'warning',
                });
              },
              complete: () => {
                Swal.fire({
                  title: 'La contraseña se actualizó correctamente.',
                  text: 'Movimiento Exitoso',
                  icon: 'success',
                });
                this.formGroup.setValue({ password: '', confirmPassword: '', currentPassword: '' });
                AjustesComponent.clic = 0;
              }
            });
          }
        }
      } else {
        alert('Las contraseñas no son iguales');
        this.confirmChangePass = false;
        AjustesComponent.clic--;
      }
    }
  }

  private startBuildForm(): void {
    this.formGroup = this.formBuilder.group({
      // tslint:disable-next-line: max-line-length
      password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}'), Validators.minLength(8), Validators.maxLength(32)]],
      confirmPassword: ['', [Validators.required]],
      currentPassword: ['']
    });
  }

  // Propiedades get
  get passwordField(): AbstractControl {
    return this.formGroup.get('password');
  }

  get confirmPasswordField(): AbstractControl {
    return this.formGroup.get('confirmPassword');
  }

  get currentPasswordField(): AbstractControl {
    return this.formGroup.get('currentPassword');
  }

}
