import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Usuario} from '../../models/usuario';
import {Router} from '@angular/router';
import {ClienteService} from '../../../../@core/services/cliente.service';
import Swal from 'sweetalert2';
import {AuthService} from '../../services';

@Component({
  selector: 'app-sign-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.scss']
})
export class UpdateFormComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  usuario: Usuario = {email: '', contrasenaApp: ''};
  result: string;
  private token;
  usuarioActivo: number;

  constructor(private fb: FormBuilder, private authService: AuthService) {
    this.loginForm = this.fb.group({
      password1: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
      password2: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
    });
    this.token = JSON.parse(localStorage.getItem('token'));
    this.usuarioActivo = this.token.id;
  }

  ngOnInit(): void {
  }
  ngOnDestroy() {
  }

  updatePassword() {
    if (this.loginForm.invalid) {
      this.result = 'Los datos son incorrectos. Por favor intentalo nuevamente.';
      return;
    } else if (this.loginForm.controls.password1.value !== this.loginForm.controls.password2.value) {
      this.result = 'Ambos campos deben ser iguales. Por favor intentalo nuevamente.';
      return;
    }
    const data = {
      contrasenaApp: this.loginForm.controls.password1.value,
      acceso: 1
    };
    this.authService.updatePassword(this.usuarioActivo, data).then((result) => {
      Swal.fire(
        'Contraseña actualizada',
        'Ahora ya puedes iniciar sesíon con tu nueva contraseña.',
        'success'
      ).then(() => {
      });
    }).catch(() => {
      this.result = 'Parece que ocurrio un error. Por favor intentalo nuevamente.';
    });
  }
}
