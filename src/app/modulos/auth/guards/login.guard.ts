import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';

import { routes } from '../../../consts';

@Injectable()
export class LoginGuard implements CanActivate{
  private routers = routes;

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const token = JSON.parse(localStorage.getItem('token'));
    // const token = localStorage.getItem('token');
    if (token && token.tipo === 1){
      this.router.navigate([this.routers.DASHBOARD]);
    } else {
      return true;
    }
  }
}
