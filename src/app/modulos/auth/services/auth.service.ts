import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { User } from '../models';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { ResponseApiData, Usuario } from '../models/usuario';
import { routes } from '../../../consts';
import { ClienteService } from "../../../@core/services/cliente.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private result: ResponseApiData;
  private routers: typeof routes = routes;
  constructor(private http: HttpClient, private router: Router, private clienteService: ClienteService) { }

  auth(formData: Usuario): Promise<{ status: number }> {
    return new Promise<{ status: number }>((resolve, reject) => {
      this.http.post<ResponseApiData>(environment.apiNode + 'login', formData).subscribe(dataResult => {
        this.result = dataResult;
      }, () => {
        reject({ status: 401 });
      }, () => {
        this.validateUserInformation(this.result);
        resolve({ status: 402 });
      });
    });
  }
  updatePassword(id, data): Promise<{ status: number }> {
    return new Promise<{ status: number }>((resolve, reject) => {
      this.http.put<{ status: number }>(environment.apiNode + 'login/' + id, data).subscribe(dataResult => {
      }, () => {
        reject({ status: 401 });
      }, () => {
        resolve({ status: 402 });
        this.router.navigate([this.routers.LOGIN]).then();
      });
    });
  }
  validateUserInformation(result: ResponseApiData): void {
    if (result.tipo === 1) {
      this.login(result);
      this.router.navigate([this.routers.DASHBOARD]).then();
    } else {
      this.login(result);
      this.router.navigateByUrl('/auth/update-password');
    }
  }
  //  ----------------- metodos de la plantilla
  public login(response: ResponseApiData): void {
    localStorage.setItem('token', JSON.stringify(response));
  }

  public sign(): void {
    localStorage.setItem('token', 'token');
  }

  public signOut(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('cliente');
  }

  public getUser(): Promise<boolean> {
    const usuario = JSON.parse(localStorage.getItem('token'));
    const datosCliente = {
      nombre: '',
      aPaterno: ''
    };
    return new Promise<boolean>((resolve, reject) => {
      this.clienteService.getCLienteById(usuario.id).subscribe(data => {
        datosCliente.nombre = data.nombre;
        datosCliente.aPaterno = data.paterno;
      }, () => {
        return reject(false);
      }, () => {
        localStorage.setItem('cliente', JSON.stringify(datosCliente));
        return resolve(true);
      });
    });
  }

  loginUser(userID: number, currentPassword: string, newPassword: string): Observable<Usuario> {
    const userData = { contrasenaApp: currentPassword, contrasenaNew: newPassword };
    return this.http.put<Usuario>(`${environment.apiNode}login/${userID}`, userData);
  }
}
