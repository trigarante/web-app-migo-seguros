import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

import { Email } from '../models';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor(
    private http: HttpClient
  ) {

  }

  public loadEmails(): Observable<Email[]> {
    return of([
      { name: 'Jane Hew', time: '9:32', message: 'Hey! How is it going?' },
      { name: 'Lloyd Brown', time: '9:18', message: 'Check out my new Dashboard' },
      { name: 'Mark Winstein', time: '9:15', message: 'I want rearrange the appointment' },
      { name: 'Liana Dutti', time: '9:09', message: 'Good news from sale department' }
    ])
  }

  getInfoEmailRecoveryPassword(email: string) {
    return this.http.get(`${environment.apiNode}get-by-email/${email}`);
  }

  sendEmailAutomatic(clientID: number) {
    // Este id se cambia por el que corresponde a cada marca
    const brandID: number = 1;
    return this.http.get(`${environment.apiMark}v1/apps/recuperar-email/${clientID}/${brandID}`);
  }

}
