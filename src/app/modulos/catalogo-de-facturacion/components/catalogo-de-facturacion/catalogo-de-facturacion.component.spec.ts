import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoDeFacturacionComponent } from './catalogo-de-facturacion.component';

describe('CatalogoDeFacturacionComponent', () => {
  let component: CatalogoDeFacturacionComponent;
  let fixture: ComponentFixture<CatalogoDeFacturacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CatalogoDeFacturacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoDeFacturacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
