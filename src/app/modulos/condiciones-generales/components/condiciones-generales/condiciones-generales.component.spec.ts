import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CondicionesGeneralesComponent } from './condiciones-generales.component';

describe('CondicionesGeneralesComponent', () => {
  let component: CondicionesGeneralesComponent;
  let fixture: ComponentFixture<CondicionesGeneralesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CondicionesGeneralesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CondicionesGeneralesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
