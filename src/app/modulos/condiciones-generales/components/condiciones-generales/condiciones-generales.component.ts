import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AseguradorasModalComponent } from '../../modal/aseguradoras-modal/aseguradoras-modal.component';
import { query } from "@angular/animations";

@Component({
  selector: 'app-condiciones-generales',
  templateUrl: 'condiciones-generales.component.html',
  styleUrls: ['./condiciones-generales.component.scss']
})
export class CondicionesGeneralesComponent implements OnInit {
  insurersGroupCollection: Array<{ id: string, urlIMG: string, urlDocument: string, cgpublico: string, cgcarga: string, cgmotos: string, showIcons: boolean }>;
  clickeado: string;
  i: number;

  constructor(public dialog: MatDialog) {
    this.insurersGroupCollection = [
      {
        id: 'ABA',
        urlIMG: 'assets/img/icons/aba.svg',
        urlDocument: 'assets/docs/condiciones-generales/aba-cg.pdf',
        cgmotos: '',
        cgpublico: '',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'SEGUROS AFIRME',
        urlIMG: 'assets/img/icons/afirme.svg',
        urlDocument: 'assets/docs/condiciones-generales/afirmecg.pdf',
        cgmotos: '',
        cgpublico: 'assets/docs/condiciones-generales/afirme_taxi.pdf',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'ANA SEGUROS',
        urlIMG: 'assets/img/icons/ana.svg',
        urlDocument: 'assets/docs/condiciones-generales/AnaSeguros.pdf',
        cgmotos: '',
        cgpublico: '',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'AXA',
        urlIMG: 'assets/img/icons/axa.svg',
        urlDocument: 'assets/docs/condiciones-generales/AXA.pdf',
        cgmotos: '',
        cgpublico: '',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'BANORTE',
        urlIMG: 'assets/img/icons/banorte.svg',
        urlDocument: 'assets/docs/condiciones-generales/banorte.pdf',
        cgmotos: '',
        cgpublico: 'assets/docs/condiciones-generales/banorte_taxi.pdf',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'GENERAL DE SEGUROS',
        urlIMG: 'assets/img/icons/general.svg',
        urlDocument: 'assets/docs/condiciones-generales/GeneralDeSeguros.pdf',
        cgmotos: '',
        cgpublico: '',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'GNP',
        urlIMG: 'assets/img/icons/gnp.svg',
        urlDocument: 'assets/docs/condiciones-generales/GNP.pdf',
        cgmotos: '',
        cgpublico: 'assets/docs/condiciones-generales/gnp_chofer.pdf',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'HDI',
        urlIMG: 'assets/img/icons/hdi.svg',
        urlDocument: 'assets/docs/condiciones-generales/HDI.pdf',
        cgmotos: '',
        cgpublico: '',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'INBURSA',
        urlIMG: 'assets/img/icons/inbursa.svg',
        urlDocument: '',
        cgmotos: '',
        cgpublico: '',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'MAPFRE',
        urlIMG: 'assets/img/icons/mapfre.svg',
        urlDocument: 'assets/docs/condiciones-generales/Mapfre.pdf',
        cgmotos: '',
        cgpublico: 'assets/docs/condiciones-generales/mapfre_tueliges.pdf',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'MIGO',
        urlIMG: 'assets/img/icons/migo.svg',
        urlDocument: '',
        cgmotos: '',
        cgpublico: '',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'EL POTOSI',
        urlIMG: 'assets/img/icons/elpotosi.svg',
        urlDocument: 'assets/docs/condiciones-generales/potosi.pdf',
        cgmotos: '',
        cgpublico: '',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'QUALITAS',
        urlIMG: 'assets/img/icons/qualitas.svg',
        urlDocument: 'assets/docs/condiciones-generales/Qualitas.pdf',
        cgmotos: 'assets/docs/condiciones-generales/qualitas_motos.pdf',
        cgpublico: 'assets/docs/condiciones-generales/qualitas_taxi.pdf',
        cgcarga: 'assets/docs/condiciones-generales/qualitas_carga.pdf',
        showIcons: true
      },
      {
        id: 'ZURA',
        urlIMG: 'assets/img/icons/sura.svg',
        urlDocument: 'assets/docs/condiciones-generales/Sura.pdf',
        cgmotos: '',
        cgpublico: '',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'ZURICH',
        urlIMG: 'assets/img/icons/zurich.svg',
        urlDocument: 'assets/docs/condiciones-generales/Zurich.pdf',
        cgmotos: '',
        cgpublico: '',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'EL AGUILA',
        urlIMG: 'assets/img/icons/elaguila.svg',
        urlDocument: 'assets/docs/condiciones-generales/aguila.pdf',
        cgmotos: '',
        cgpublico: '',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'AIG',
        urlIMG: 'assets/img/icons/aig.svg',
        urlDocument: 'assets/docs/condiciones-generales/AIG.pdf',
        cgmotos: '',
        cgpublico: '',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'LA LATINO',
        urlIMG: 'assets/img/icons/lalatino.svg',
        urlDocument: 'assets/docs/condiciones-generales/LaLatino.pdf',
        cgmotos: '',
        cgpublico: 'assets/docs/condiciones-generales/lalatino_taxi.pdf',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'Atlas Seguros',
        urlIMG: 'assets/img/icons/atlas-seguros.svg',
        urlDocument: 'assets/docs/condiciones-generales/atlasSeguros.pdf',
        cgmotos: '',
        cgpublico: '',
        cgcarga: '',
        showIcons: true
      },
      {
        id: 'Bx+',
        urlIMG: 'assets/img/icons/bxmas2.svg',
        urlDocument: 'assets/docs/condiciones-generales/Bx+.pdf',
        cgmotos: '',
        cgpublico: '',
        cgcarga: '',
        showIcons: true
      }
    ];
  }

  openDialog(i) {
    if (!this.insurersGroupCollection[i].cgmotos && !this.insurersGroupCollection[i].cgpublico && !this.insurersGroupCollection[i].cgcarga) {
      this.clickeado = this.insurersGroupCollection[i].urlDocument;
      const dialogRef = this.dialog.open(AseguradorasModalComponent, { data: { route: this.clickeado } });
      dialogRef.afterClosed().subscribe(result => { });
    }
    else {
      this.insurersGroupCollection[i].showIcons = !this.insurersGroupCollection[i].showIcons;
    }
  }

  openGeneral(i) {
    this.clickeado = this.insurersGroupCollection[i].urlDocument;
    const dialogRef = this.dialog.open(AseguradorasModalComponent, { data: { route: this.clickeado } });
    dialogRef.afterClosed().subscribe(result => { });
  }

  openTaxi(i) {
    this.clickeado = this.insurersGroupCollection[i].cgpublico;
    const dialogRef = this.dialog.open(AseguradorasModalComponent, { data: { route: this.clickeado } });
    dialogRef.afterClosed().subscribe(result => { });
  }

  openCarga(i) {
    this.clickeado = this.insurersGroupCollection[i].cgcarga;
    const dialogRef = this.dialog.open(AseguradorasModalComponent, { data: { route: this.clickeado } });
    dialogRef.afterClosed().subscribe(result => { });
  }

  openMoto(i) {
    this.clickeado = this.insurersGroupCollection[i].cgmotos;
    const dialogRef = this.dialog.open(AseguradorasModalComponent, { data: { route: this.clickeado } });

    dialogRef.afterClosed().subscribe(result => { });
  }
  ngOnInit(): void {
  }
}
