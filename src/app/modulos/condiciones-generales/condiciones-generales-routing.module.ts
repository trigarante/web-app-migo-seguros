import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CondicionesGeneralesPageComponent} from './containers/condiciones-generales-page/condiciones-generales-page.component';

const routes: Routes = [
  {
    path: '',
    component: CondicionesGeneralesPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CondicionesGeneralesRoutingModule { }
