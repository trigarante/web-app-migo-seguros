import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-aseguradoras-modal',
  templateUrl: './aseguradoras-modal.component.html',
  styleUrls: ['./aseguradoras-modal.component.scss']
})
export class AseguradorasModalComponent implements OnInit {
  documentRoute: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: {route: string}) {
    this.documentRoute = data.route;
  }

  ngOnInit(): void {
  }

}
