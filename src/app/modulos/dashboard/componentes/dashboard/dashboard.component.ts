import {Component, Input, OnInit} from '@angular/core';
import {ClienteClases} from '../../../../@core/clases/clienteClases';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{
  saludo = '';
  user: string;
  usernames: string[];
  dateLive: Date;
  clienteClase = new ClienteClases();
  urlPDF = 'assets/docs/FolletoExplicativo.pdf';
  constructor() {
    this.clienteClase.validarExisteCliente().then(() => {
      this.user = JSON.parse(localStorage.getItem('cliente')).nombre;
    });
  }
  async ngOnInit() {
    this.mostrarSaludo();
    // this.particlesFunction();
    this.dateLive = new Date();
  }

  mostrarSaludo(): void{
    const fecha = new Date();
    const hora = fecha.getHours();

    if (hora >= 0 && hora < 12){
      this.saludo = 'buenos días';
    }

    if (hora >= 12 && hora < 18){
      this.saludo = 'buenas tardes';
    }

    if (hora >= 18 && hora < 24){
      this.saludo = 'buenas noches';
    }
  }

  // particlesFunction(): void {
  //   tsParticles.load('tsparticles', {
  //     fpsLimit: 60,
  //     background: {
  //       color: '#ffffff'
  //     },
  //     backgroundMode: {
  //       enable: true
  //     },
  //     particles: {
  //       color: {
  //         value: ['#5053cd', '#e71958']
  //       },
  //       links: {
  //         color: '#621940',
  //         enable: true
  //       },
  //       move: {
  //         enable: true,
  //         speed: 6
  //       },
  //       size: {
  //         value: 5,
  //         random: {
  //           enable: true,
  //           minimumValue: 1
  //         },
  //         animation: {
  //           enable: true,
  //           speed: 2.5,
  //           minimumValue: 1
  //         }
  //       },
  //       opacity: {
  //         value: 0.8,
  //         random: {
  //           enable: true,
  //           minimumValue: 0.4
  //         }
  //       }
  //     }
  //   });
  // }
}
