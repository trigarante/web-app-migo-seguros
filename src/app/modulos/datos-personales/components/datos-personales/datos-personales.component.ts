import { Component, OnInit } from '@angular/core';
import {ClienteService} from '../../../../@core/services/cliente.service';
import {Cliente} from '../../../../@core/interfaces/cliente';

@Component({
  selector: 'app-datos-personales',
  templateUrl: './datos-personales.component.html',
  styleUrls: ['./datos-personales.component.scss']
})
export class DatosPersonalesComponent implements OnInit {
  usuarioActivo: number;
  cliente: Cliente;
  nombre = '';
  usuario;
  loaderCondition = false;
  constructor(private clienteService: ClienteService) {
    this.usuario = JSON.parse(localStorage.getItem('token'));
    this.usuarioActivo = this.usuario.id;
  }

  ngOnInit() {
    this.getClienteById();
  }
  getClienteById() {
    this.clienteService.getCLienteById(this.usuarioActivo).subscribe(data => {
      this.nombre = data.nombre + ' ' + data.paterno + ' ' + data.materno;
      this.cliente = data;
      this.loaderCondition = true;
    });
  }

}
