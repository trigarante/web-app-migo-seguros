import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { DatosPersonalesPageComponent } from './containers';

const routes: Routes = [
  {
    path: '',
    component: DatosPersonalesPageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class DatosPersonalesRoutingModule {
}
