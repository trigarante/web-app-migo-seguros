import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { EmailService } from 'src/app/modulos/auth/services';

@Component({
  selector: 'app-mail-field',
  templateUrl: './mail-field.component.html',
  styleUrls: ['./mail-field.component.scss']
})
export class MailFieldComponent implements OnInit {
  loginForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private dialog: MatDialogRef<MailFieldComponent>,
    private emailService: EmailService
  ) {
    this.loginForm = this.fb.group({
      email: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
      contrasenaApp: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
      activo: 1,
      idMarcaEmpresa: 1
    });
  }

  ngOnInit(): void {
  }


  validateEmail() {

    let idCliente: number;

    this.emailService.getInfoEmailRecoveryPassword(this.loginForm.get('email').value).subscribe({
      next: (dataResponse: any) => {
        idCliente = dataResponse.id;
      },
      error: (error) => {
        console.error('Error al validar el correo electrónico');
      },
      complete: () => {
        this.sendEmail(idCliente);
      }
    });
    // this.closeDialog.close();
  }

  private sendEmail(clientID: number) {
    let validate = false;
    this.emailService.sendEmailAutomatic(clientID).subscribe({
      next: (dataResponse) => {
        validate = true;
      },
      error: (error) => {
        validate = false;
        console.error('Error al enviar el correo => ', error);
      },
      complete: () => {
        this.dialog.close(validate);
      }
    });
  }

}
