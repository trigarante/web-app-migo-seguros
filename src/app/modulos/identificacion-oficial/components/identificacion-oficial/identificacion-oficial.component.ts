import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-identificacion-oficial',
  templateUrl: './identificacion-oficial.component.html',
  styleUrls: ['./identificacion-oficial.component.scss']
})
export class IdentificacionOficialComponent implements OnInit {
  folders = [
    {
      name: 'INE',
      updated: new Date('1/1/16'),
    },
    {
      name: 'Pasaporte vigente',
      updated: new Date('1/17/16'),
    },
    {
      name: 'Cédula profesional',
      updated: new Date('1/28/16'),
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
