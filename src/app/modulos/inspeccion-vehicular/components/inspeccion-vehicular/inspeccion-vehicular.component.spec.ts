import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InspeccionVehicularComponent } from './inspeccion-vehicular.component';

describe('InspeccionVehicularComponent', () => {
  let component: InspeccionVehicularComponent;
  let fixture: ComponentFixture<InspeccionVehicularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InspeccionVehicularComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InspeccionVehicularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
