import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InspeccionVehicularPageComponent } from './inspeccion-vehicular-page.component';

describe('InspeccionVehicularPageComponent', () => {
  let component: InspeccionVehicularPageComponent;
  let fixture: ComponentFixture<InspeccionVehicularPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InspeccionVehicularPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InspeccionVehicularPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
