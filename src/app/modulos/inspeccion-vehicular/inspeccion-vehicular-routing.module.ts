import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {InspeccionVehicularPageComponent} from "./containers";

const routes: Routes = [
  {path: '',
    component: InspeccionVehicularPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InspeccionVehicularRoutingModule { }
