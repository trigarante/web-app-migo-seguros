import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstruccionesDePagoPageComponent } from './instrucciones-de-pago-page.component';

describe('InstruccionesDePagoPageComponent', () => {
  let component: InstruccionesDePagoPageComponent;
  let fixture: ComponentFixture<InstruccionesDePagoPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstruccionesDePagoPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstruccionesDePagoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
