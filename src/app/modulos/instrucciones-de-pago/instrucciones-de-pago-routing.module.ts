import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {InstruccionesDePagoPageComponent} from './containers';

const routes: Routes = [
  {
    path: '',
    component: InstruccionesDePagoPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstruccionesDePagoRoutingModule { }
