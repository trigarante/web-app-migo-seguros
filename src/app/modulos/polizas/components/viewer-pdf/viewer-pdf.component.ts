import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PolizaService} from '../../../../@core/services/poliza.service';
import {routes} from '../../../../consts';

@Component({
  selector: 'app-viewer-pdf',
  templateUrl: './viewer-pdf.component.html',
  styleUrls: ['./viewer-pdf.component.scss']
})
export class ViewerPdfComponent implements OnInit{

  archivoPoliza: string;
  idCliente: string;
  idRegistro: string;
  downloadButton: boolean = false;

  urlPDF = 'https://app.mark-43.net/auth-cliente/poliza/';

  private routers: typeof routes = routes;
    
  constructor(
    private route: ActivatedRoute, 
    private polizaService: PolizaService, 
    private router: Router
    ) {
    this.archivoPoliza = String(this.route.snapshot.paramMap.get('idArchivo'));
    this.idCliente = String(this.route.snapshot.paramMap.get('idCliente'));
    this.idRegistro = String(this.route.snapshot.paramMap.get('idRegistro'));
    this.urlPDF += this.archivoPoliza;
  }

  ngOnInit() {
  }
  async getPoliza() {
    try {
      this.polizaService.getDocument(this.archivoPoliza, this.idCliente, this.idRegistro).subscribe({
        next: (data) => { },
        error: (error) => { console.error('Error al obtener el documento => ', error); },
        complete: () => { window.open(this.urlPDF, '_blank'); }
      });
    } catch (err) {
      console.error('Error catch => ', err.message);
    }
  }
  
  pageRendered() {
    this.downloadButton = true;
  }
}
