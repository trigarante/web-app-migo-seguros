import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PolizasPageComponent } from './containers';
import {ViewerPdfComponent} from './components/viewer-pdf/viewer-pdf.component';

const routes: Routes = [
  {
    path: '',
    component: PolizasPageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class PolizasRoutingModule {
}
