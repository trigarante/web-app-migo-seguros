import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';

import { PolizasPageComponent } from './containers';
import { PolizasRoutingModule } from './polizas-routing.module';
import { SharedModule } from '../../shared/shared.module';
import {PolizasComponent} from './components/polizas/polizas.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDialogModule} from '@angular/material/dialog';
import { ViewerPdfComponent } from './components/viewer-pdf/viewer-pdf.component';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import { DetalleRegistroComponent } from './components/detalle-registro/detalle-registro.component';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {_MatMenuDirectivesModule, MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  declarations: [PolizasPageComponent, PolizasComponent, ViewerPdfComponent, DetalleRegistroComponent],
  imports: [
    CommonModule,
    PolizasRoutingModule,
    MatCardModule,
    MatToolbarModule,
    SharedModule,
    MatDialogModule,
    MatProgressBarModule,
    PdfViewerModule,
    MatButtonModule,
    MatTooltipModule,
    _MatMenuDirectivesModule,
    MatIconModule,
    MatProgressSpinnerModule
  ]
})
export class PolizasModule { }
