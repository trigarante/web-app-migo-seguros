import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';

import { SolicitarModificacionesPageComponent } from './containers';
import { SolicitarModificacionesRoutingModule } from './solicitar-modificaciones-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { SolicitarModificacionesComponent } from './components/solicitar-modificaciones/solicitar-modificaciones.component';
import {MatListModule} from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';

@NgModule({
  declarations: [SolicitarModificacionesPageComponent, SolicitarModificacionesComponent],
  imports: [
    CommonModule,
    SolicitarModificacionesRoutingModule,
    MatCardModule,
    MatToolbarModule,
    SharedModule,
    MatListModule,
    MatExpansionModule,
  ]
})
export class SolicitarModificacionesModule { }
