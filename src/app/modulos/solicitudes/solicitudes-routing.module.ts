import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { SolicitudesPageComponent } from './containers';

const routes: Routes = [
  {
    path: '',
    component: SolicitudesPageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class SolicitudesRoutingModule {
}
