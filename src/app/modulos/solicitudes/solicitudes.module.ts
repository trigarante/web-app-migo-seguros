import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';

import { SolicitudesPageComponent } from './containers';
import { SolicitudesRoutingModule } from './solicitudes-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { SolicitudesComponent } from './components/solicitudes/solicitudes.component';

@NgModule({
  declarations: [SolicitudesPageComponent, SolicitudesComponent],
  imports: [
    CommonModule,
    SolicitudesRoutingModule,
    MatCardModule,
    MatToolbarModule,
    SharedModule
  ]
})
export class SolicitudesModule { }
