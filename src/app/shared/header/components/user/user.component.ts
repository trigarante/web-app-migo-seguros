import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  AfterViewChecked,
  AfterContentChecked,
  HostListener
} from '@angular/core';

import { routes } from '../../../../consts';
import {AuthService} from '../../../../modulos/auth/services';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, AfterContentChecked{
  @Output() signOut: EventEmitter<void> = new EventEmitter<void>();
  public routes: typeof routes = routes;
  nombreCliente: string;
  separateName: string[];
  firstName: string;
  loaderCondition = false;
  public signOutEmit(): void {
    this.signOut.emit();
  }

  constructor(private userService: AuthService) {
    this.nombreCliente = localStorage.getItem('cliente') !== null ? JSON.parse(localStorage.getItem('cliente')).nombre : '';
  }
  ngOnInit() {
    this.getClienteById();
  }
  ngAfterContentChecked(){
    if (this.nombreCliente) {
      this.firstName = this.nombreCliente.toLowerCase();
      this.separateName = this.firstName.split(' ');
      this.firstName = this.separateName[0][0].toUpperCase() + this.separateName[0].slice(1);
      this.loaderCondition = true;
    }
  }

  getClienteById() {
    const cliente = localStorage.getItem('cliente');
    if (cliente === null) {
      this.userService.getUser().catch(() => {}).then(() => {
        this.nombreCliente = JSON.parse(localStorage.getItem('cliente')).nombre;
      });
    }
  }
}
