import { Component, EventEmitter, Input, Output, HostListener, OnInit, AfterContentInit} from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { Email, User } from '../../../../modulos/auth/models';
import { AuthService, EmailService } from '../../../../modulos/auth/services';
import { routes } from '../../../../consts';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements AfterContentInit{
  @Input() isMenuOpened: boolean;
  @Output() isShowSidebar = new EventEmitter<boolean>();
  public routers: typeof routes = routes;
  innerWidth: number;


  ngAfterContentInit() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth >= 900) {
      this.isShowSidebar.emit(false);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth >= 900) {
      this.isShowSidebar.emit(false);
    }
  }
  constructor(public userService: AuthService, public router: Router){
  }

  public openMenu(): void {
    if (this.innerWidth < 900) {
      this.isMenuOpened = !this.isMenuOpened;
      this.isShowSidebar.emit(this.isMenuOpened);
    }
    else{
      this.isShowSidebar.emit(false);
    }
  }
  public signOut(): void {
      this.userService.signOut();
      this.router.navigate([this.routers.LOGIN]);
  }
}
