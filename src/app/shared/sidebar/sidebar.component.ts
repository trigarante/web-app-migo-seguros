import {Component, OnChanges, OnInit} from '@angular/core';
import {routes} from '../../consts/routes';
import {AuthService} from '../../modulos/auth/services';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit{
  public routes: typeof routes = routes;
  public isOpenUiElements = false;
  public userMenu  = false;
  url: string;
  color: string;
  transform: string;
  constructor(public userService: AuthService, public router: Router){
  }

  ngOnInit(){
    this.url = window.location.hash;
    if ('#' + this.routes.CONDICIONES_GENERALES === this.url
      || '#' + this.routes.ACLARACION_Y_SUGERENCIAS === this.url
      || '#' + this.routes.CANCELAR_UNA_POLIZA === this.url
      || '#' + this.routes.INSTRUCCIONES_DE_PAGO === this.url
      || '#' + this.routes.CATALOGO_DE_FACTURACION === this.url ){
      this.isOpenUiElements = true;
    }
    else if ('#' + this.routes.CAMBIAR_CONTRASENA === this.url
          || '#' + this.routes.DATOS_PERSONALES === this.url)
          {
            this.userMenu = true;
          }
  }
  transformStyles(){
    if (this.isOpenUiElements === true){
      this.transform = 'rotate(-0.5turn)';
    }else {
      this.transform = 'rotate(0turn)';
    }
    return this.transform;
  }
  changesStyles(){
    if (this.isOpenUiElements === true){
      this.color = '#00000021';
    }
    else{
      this.color = 'transparent';
    }
    return this.color;
  }
  transformStylesUM(){
    if (this.userMenu === true){
      this.transform = 'rotate(-0.5turn)';
    }else {
      this.transform = 'rotate(0turn)';
    }
    return this.transform;
  }
  changesStylesUM(){
    if (this.userMenu === true){
      this.color = '#00000021';
    }
    else{
      this.color = 'transparent';
    }
    return this.color;
  }
  public openUiElements() {
    this.isOpenUiElements = !this.isOpenUiElements;
  }
  public openUserMenu() {
    this.userMenu = !this.userMenu;
  }
  public signOut(): void {
    this.userService.signOut();
    this.router.navigate([this.routes.LOGIN]);
  }
}
